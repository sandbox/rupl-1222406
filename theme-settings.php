<?php

// If the Adapt folder exists, include theme registry functions and logic.
// This is designed so that you can delete the Adapt folder and completely remove Adapt integration from NineSixty
$adapt_registry_path = drupal_get_path('theme', 'ninesixty') .'/adapt/template.theme-registry.inc';
if (file_exists($adapt_registry_path)) {
  include_once $adapt_registry_path;
}

/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   array An array of saved settings for this theme.
 * @return
 *   array A form array.
 */
function ninesixty_settings($saved_settings) {

  // Get default theme settings.
  $defaults = ninesixty_get_default_settings();

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  $form['ninesixty_adapt_active'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Activate adapt.js'),
    '#default_value'  => $settings['ninesixty_adapt_active'],
  );
  $form['ninesixty_adapt'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Adapt.js settings'),
    '#attributes'     => array(
      'id'            => 'ninesixty-adapt-settings',
    ),
  );
  $form['ninesixty_adapt']['ninesixty_adapt_dynamic'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Acitivate responsive grids'),
    '#default_value'  => $settings['ninesixty_adapt_dynamic'],
    '#return_value'   => 'true',
    '#description'    => t('When <em>dynamic</em> is selected, adapt.js will react to window resizing or orientation changes.'),
  );
  $form['ninesixty_adapt']['ninesixty_adapt_path'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Path to the "adapt" folder'),
    '#field_prefix'   => drupal_get_path('theme', 'ninesixty') .'/',
    '#default_value'  => $settings['ninesixty_adapt_path'],
    '#description'    => t('Include trailing slash'),
  );
  $form['ninesixty_adapt']['ninesixty_adapt_range'] = array(
    '#type'           => 'textarea',
    '#title'          => t('The ranges you want to serve'),
    '#default_value'  => $settings['ninesixty_adapt_range'],
    '#description'    => t('Please refer to <a href="http://adapt.960.gs" target="_blank">http://adapt.960.gs</a> for help with the required format.'),
    '#rows' => 7,
  );
  $form['ninesixty_adapt']['ninesixty_adapt_noscript'] = array(
    '#type'           => 'textfield',
    '#title'          => t('&lt;noscript&gt; stylesheet'),
    '#field_prefix'   => drupal_get_path('theme', 'ninesixty') .'/'. $settings['ninesixty_adapt_path'],
    '#default_value'  => $settings['ninesixty_adapt_noscript'],
    '#description'    => t('If the user has JavaScript is disabled, this will be the only stylesheet loaded. It should be a mobile stylesheet.'),
  );

  // Return the additional form widgets
  return $form;
}

