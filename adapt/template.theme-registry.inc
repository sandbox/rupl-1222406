<?php

/*
 * Return array of default values.
 */
function ninesixty_get_default_settings() {
  return array(
    'ninesixty_adapt_active'    => FALSE,
    'ninesixty_adapt_path'      => 'adapt/assets/css/', // relative to NineSixty directory
    'ninesixty_adapt_dynamic'   => 'true',
    'ninesixty_adapt_range'     => "'0px    to 760px  = mobile.css',
'760px  to 980px  = 720.css',
'980px  to 1280px = 960.css',
'1280px to 1600px = 1200.css',
'1600px to 1920px = 1560.css',
'1920px           = fluid.css'",
    'ninesixty_adapt_noscript'  => 'mobile.min.css',
  );
}

/*
 * Initialize theme settings.
 *
 * Called from hook_theme() when the registry is rebuilt.
 * For more information, see http://drupal.org/node/177868
 */
function ninesixty_initialize_theme_settings() {
  if (is_null(theme_get_setting('ninesixty_adapt_active'))) {
    global $theme_key;

    // Get default theme settings.
    $defaults = ninesixty_get_default_settings();

    // Get the stored theme settings.
    $settings = theme_get_settings($theme_key);

    // Don't save the toggle_node_info_ variables.
    if (module_exists('node')) {
      foreach (node_get_types() as $type => $name) {
        unset($settings['toggle_node_info_' . $type]);
      }
    }

    // Save default theme settings.
    variable_set(
      str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
      array_merge($defaults, $settings)
    );

    // Force refresh of Drupal internals.
    theme_get_setting('', TRUE);
  }
}
